<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A84622">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A panegyrick to His Renowed [sic] Majestie, Charles the Second, King of Great Britaine, &amp;c.</title>
    <author>Flatman, Thomas, 1637-1688.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A84622 of text R212460 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.25[51]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A84622</idno>
    <idno type="STC">Wing F1149</idno>
    <idno type="STC">Thomason 669.f.25[51]</idno>
    <idno type="STC">ESTC R212460</idno>
    <idno type="EEBO-CITATION">99871075</idno>
    <idno type="PROQUEST">99871075</idno>
    <idno type="VID">163856</idno>
    <idno type="PROQUESTGOID">2240929327</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A84622)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163856)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f25[51])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A panegyrick to His Renowed [sic] Majestie, Charles the Second, King of Great Britaine, &amp;c.</title>
      <author>Flatman, Thomas, 1637-1688.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for Henry Marsh at the Princes Arms in Chancery-lane near Fleetstreet,</publisher>
      <pubPlace>London :</pubPlace>
      <date>MDCLX. [1660]</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed at end: T.F. (i.e. Thomas Flatman).</note>
      <note>Verse - "Return, return, strange Prodigie of Fate!".</note>
      <note>Annotation on Thomason copy: "June 30 [illegible]".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Charles -- II, -- King of England, 1630-1685 -- Poetry -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A84622</ep:tcp>
    <ep:estc> R212460</ep:estc>
    <ep:stc> (Thomason 669.f.25[51]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>A panegyrick to His Renowed [sic] Majestie, Charles the Second, King of Great Britaine, &amp;c.</ep:title>
    <ep:author>Flatman, Thomas</ep:author>
    <ep:publicationYear>1660</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>325</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-09</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-10</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change><date>2007-10</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A84622-e10">
  <body xml:id="A84622-e20">
   <pb facs="tcp:163856:1" rend="simple:additions" xml:id="A84622-001-a"/>
   <div type="text" xml:id="A84622-e30">
    <head xml:id="A84622-e40">
     <w lemma="a" pos="d" xml:id="A84622-001-a-0010">A</w>
     <w lemma="panegyric" pos="n1" reg="panegyric" xml:id="A84622-001-a-0020">PANEGYRICK</w>
     <w lemma="to" pos="acp" xml:id="A84622-001-a-0030">To</w>
     <w lemma="his" pos="po" xml:id="A84622-001-a-0040">His</w>
     <w lemma="renown" pos="j-vn" reg="renowned" xml:id="A84622-001-a-0050">Renowed</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A84622-001-a-0060">MAjESTIE</w>
     <pc xml:id="A84622-001-a-0070">,</pc>
     <w lemma="Charles" pos="nn1" xml:id="A84622-001-a-0080">Charles</w>
     <w lemma="the" pos="d" xml:id="A84622-001-a-0090">the</w>
     <w lemma="second" pos="ord" xml:id="A84622-001-a-0100">Second</w>
     <pc xml:id="A84622-001-a-0110">,</pc>
     <w lemma="king" pos="n1" xml:id="A84622-001-a-0120">King</w>
     <w lemma="of" pos="acp" xml:id="A84622-001-a-0130">of</w>
     <w lemma="great" pos="j" xml:id="A84622-001-a-0140">Great</w>
     <hi xml:id="A84622-e50">
      <w lemma="Britain" pos="nn1" reg="Britain" xml:id="A84622-001-a-0150">Britaine</w>
      <pc xml:id="A84622-001-a-0160">,</pc>
     </hi>
     <w lemma="etc." pos="ab" xml:id="A84622-001-a-0170">&amp;c.</w>
     <pc unit="sentence" xml:id="A84622-001-a-0180"/>
    </head>
    <lg xml:id="A84622-e60">
     <l xml:id="A84622-e70">
      <w lemma="return" pos="vvb" rend="decorinit" xml:id="A84622-001-a-0190">REturn</w>
      <pc xml:id="A84622-001-a-0200">,</pc>
      <w lemma="return" pos="vvb" xml:id="A84622-001-a-0210">return</w>
      <pc xml:id="A84622-001-a-0220">,</pc>
      <w lemma="strange" pos="j" xml:id="A84622-001-a-0230">strange</w>
      <w lemma="prodigy" pos="n1" reg="prodigy" xml:id="A84622-001-a-0240">Prodigie</w>
      <w lemma="of" pos="acp" xml:id="A84622-001-a-0250">of</w>
      <w lemma="fate" pos="n1" xml:id="A84622-001-a-0260">Fate</w>
      <pc unit="sentence" xml:id="A84622-001-a-0270">!</pc>
     </l>
     <l xml:id="A84622-e80">
      <w lemma="gird" pos="vvb" xml:id="A84622-001-a-0280">Gird</w>
      <w lemma="on" pos="acp" xml:id="A84622-001-a-0290">on</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-0300">thy</w>
      <w lemma="beam" pos="n2" xml:id="A84622-001-a-0310">Beams</w>
      <pc xml:id="A84622-001-a-0320">,</pc>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-0330">and</w>
      <w lemma="reassume" pos="vvb" reg="reassume" xml:id="A84622-001-a-0340">re-assume</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-0350">thy</w>
      <w lemma="state" pos="n1" xml:id="A84622-001-a-0360">State</w>
      <pc unit="sentence" xml:id="A84622-001-a-0370">.</pc>
     </l>
     <l xml:id="A84622-e90">
      <w lemma="miraculous" pos="j" xml:id="A84622-001-a-0380">Miraculous</w>
      <w lemma="prince" pos="n1" xml:id="A84622-001-a-0390">Prince</w>
      <pc xml:id="A84622-001-a-0400">,</pc>
      <w lemma="beyond" pos="acp" xml:id="A84622-001-a-0410">beyond</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-0420">the</w>
      <w lemma="reach" pos="n1" xml:id="A84622-001-a-0430">reach</w>
      <w lemma="of" pos="acp" xml:id="A84622-001-a-0440">of</w>
      <w lemma="verse" pos="n1" xml:id="A84622-001-a-0450">Verse</w>
      <pc xml:id="A84622-001-a-0460">,</pc>
     </l>
     <l xml:id="A84622-e100">
      <w lemma="the" pos="d" xml:id="A84622-001-a-0470">The</w>
      <w lemma="fame" pos="n1" xml:id="A84622-001-a-0480">Fame</w>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-0490">and</w>
      <w lemma="wonder" pos="n1" xml:id="A84622-001-a-0500">Wonder</w>
      <w lemma="of" pos="acp" xml:id="A84622-001-a-0510">of</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-0520">the</w>
      <w lemma="universe" pos="n1" xml:id="A84622-001-a-0530">Universe</w>
      <pc unit="sentence" xml:id="A84622-001-a-0540">!</pc>
     </l>
     <l xml:id="A84622-e110">
      <w lemma="preserve" pos="vvn" reg="Preserved" xml:id="A84622-001-a-0550">Preserv'd</w>
      <w lemma="by" pos="acp" xml:id="A84622-001-a-0560">by</w>
      <w lemma="a" pos="d" xml:id="A84622-001-a-0570">an</w>
      <w lemma="almighty" pos="j" xml:id="A84622-001-a-0580">Almighty</w>
      <w lemma="hand" pos="n1" xml:id="A84622-001-a-0590">hand</w>
      <pc xml:id="A84622-001-a-0600">,</pc>
      <w lemma="when" pos="crq" xml:id="A84622-001-a-0610">when</w>
      <hi xml:id="A84622-e120">
       <w lemma="Rome" pos="nn1" xml:id="A84622-001-a-0620">Rome</w>
       <pc xml:id="A84622-001-a-0630">,</pc>
      </hi>
     </l>
     <l xml:id="A84622-e130">
      <w lemma="and" pos="cc" xml:id="A84622-001-a-0640">And</w>
      <w lemma="rage" pos="vvg" xml:id="A84622-001-a-0650">raging</w>
      <hi xml:id="A84622-e140">
       <w lemma="Oliver" pos="nn1" xml:id="A84622-001-a-0660">Oliver</w>
      </hi>
      <w lemma="have" pos="vvd" xml:id="A84622-001-a-0670">had</w>
      <w lemma="read" pos="vvn" xml:id="A84622-001-a-0680">read</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-0690">thy</w>
      <w lemma="doom" pos="n1" xml:id="A84622-001-a-0700">doom</w>
      <pc unit="sentence" xml:id="A84622-001-a-0710">!</pc>
     </l>
     <l xml:id="A84622-e150">
      <w lemma="deliver" pos="vvn" reg="Delivered" xml:id="A84622-001-a-0720">Deliver'd</w>
      <w lemma="from" pos="acp" xml:id="A84622-001-a-0730">from</w>
      <w lemma="a" pos="d" xml:id="A84622-001-a-0740">a</w>
      <w lemma="bloody" pos="j" reg="bloody" xml:id="A84622-001-a-0750">bloudy</w>
      <hi xml:id="A84622-e160">
       <w lemma="junto" pos="n1" xml:id="A84622-001-a-0760">Junto</w>
      </hi>
      <pc join="right" xml:id="A84622-001-a-0770">(</pc>
      <w lemma="man" pos="n2" xml:id="A84622-001-a-0780">men</w>
      <pc xml:id="A84622-001-a-0790">,</pc>
     </l>
     <l xml:id="A84622-e170">
      <w lemma="that" pos="cs" xml:id="A84622-001-a-0800">That</w>
      <w lemma="glad" pos="av-j" xml:id="A84622-001-a-0810">gladly</w>
      <w lemma="will" pos="vmd" xml:id="A84622-001-a-0820">would</w>
      <w lemma="be" pos="vvi" xml:id="A84622-001-a-0830">be</w>
      <w lemma="murderer" pos="n2" reg="murderers" xml:id="A84622-001-a-0840">Murtherers</w>
      <w lemma="again" pos="av" reg="again" xml:id="A84622-001-a-0850">agen</w>
      <pc xml:id="A84622-001-a-0860">!</pc>
      <pc unit="sentence" xml:id="A84622-001-a-0870">)</pc>
     </l>
     <l xml:id="A84622-e180">
      <w lemma="thy" pos="po" xml:id="A84622-001-a-0880">Thy</w>
      <w lemma="valiant" pos="j" xml:id="A84622-001-a-0890">valiant</w>
      <w lemma="arm" pos="n2" xml:id="A84622-001-a-0900">Arms</w>
      <w lemma="have" pos="vvb" xml:id="A84622-001-a-0910">have</w>
      <w lemma="struggle" pos="vvn" reg="struggled" xml:id="A84622-001-a-0920">strugled</w>
      <w lemma="with" pos="acp" xml:id="A84622-001-a-0930">with</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-0940">the</w>
      <w lemma="tide" pos="n1" xml:id="A84622-001-a-0950">Tide</w>
      <pc xml:id="A84622-001-a-0960">,</pc>
     </l>
     <l xml:id="A84622-e190">
      <w lemma="encounter" pos="vvd" reg="Encountered" xml:id="A84622-001-a-0970">Encountred</w>
      <w lemma="all" pos="d" xml:id="A84622-001-a-0980">all</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-0990">the</w>
      <w lemma="wind" pos="n2" xml:id="A84622-001-a-1000">Winds</w>
      <pc xml:id="A84622-001-a-1010">,</pc>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-1020">and</w>
      <w lemma="scorn" pos="vvd" reg="scorned" xml:id="A84622-001-a-1030">scorn'd</w>
      <w lemma="their" pos="po" xml:id="A84622-001-a-1040">their</w>
      <w lemma="pride" pos="n1" xml:id="A84622-001-a-1050">Pride</w>
      <pc xml:id="A84622-001-a-1060">:</pc>
     </l>
     <l xml:id="A84622-e200">
      <w lemma="guard" pos="vvn" xml:id="A84622-001-a-1070">Guarded</w>
      <w lemma="with" pos="acp" xml:id="A84622-001-a-1080">with</w>
      <w lemma="angel" pos="n2" xml:id="A84622-001-a-1090">Angels</w>
      <pc xml:id="A84622-001-a-1100">;</pc>
      <w lemma="yet" pos="av" xml:id="A84622-001-a-1110">yet</w>
      <w lemma="preserve" pos="vvn" reg="preserved" xml:id="A84622-001-a-1120">preserv'd</w>
      <w lemma="to" pos="prt" xml:id="A84622-001-a-1130">to</w>
      <w lemma="be" pos="vvi" xml:id="A84622-001-a-1140">be</w>
     </l>
     <l xml:id="A84622-e210">
      <w lemma="distract" pos="vvn" xml:id="A84622-001-a-1150">Distracted</w>
      <pc xml:id="A84622-001-a-1160">,</pc>
      <w lemma="heartsick" pos="j" reg="heartsick" xml:id="A84622-001-a-1170">heart-sick</w>
      <hi xml:id="A84622-e220">
       <w lemma="England" pos="nng1" xml:id="A84622-001-a-1180">England's</w>
      </hi>
      <w lemma="remedy" pos="n1" reg="remedy" xml:id="A84622-001-a-1190">Remedie</w>
      <pc unit="sentence" xml:id="A84622-001-a-1200">!</pc>
     </l>
     <l xml:id="A84622-e230">
      <w lemma="come" pos="vvb" xml:id="A84622-001-a-1210">Come</w>
      <pc xml:id="A84622-001-a-1220">,</pc>
      <w lemma="royal" pos="j" xml:id="A84622-001-a-1230">Royal</w>
      <w lemma="exile" pos="n1" xml:id="A84622-001-a-1240">Exile</w>
      <pc unit="sentence" xml:id="A84622-001-a-1250">!</pc>
      <w lemma="we" pos="pns" xml:id="A84622-001-a-1260">We</w>
      <w lemma="submit" pos="vvb" xml:id="A84622-001-a-1270">submit</w>
      <pc xml:id="A84622-001-a-1280">,</pc>
      <w lemma="we" pos="pns" xml:id="A84622-001-a-1290">we</w>
      <w lemma="fall" pos="vvb" xml:id="A84622-001-a-1300">fall</w>
      <pc xml:id="A84622-001-a-1310">,</pc>
     </l>
     <l xml:id="A84622-e240">
      <w lemma="we" pos="pns" xml:id="A84622-001-a-1320">We</w>
      <w lemma="bend" pos="vvb" xml:id="A84622-001-a-1330">bend</w>
      <w lemma="before" pos="acp" xml:id="A84622-001-a-1340">before</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-1350">thy</w>
      <w lemma="throne" pos="n1" xml:id="A84622-001-a-1360">Throne</w>
      <pc xml:id="A84622-001-a-1370">,</pc>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-1380">and</w>
      <w lemma="give" pos="vvi" xml:id="A84622-001-a-1390">give</w>
      <w lemma="thou" pos="pno" xml:id="A84622-001-a-1400">thee</w>
      <w lemma="all" pos="d" xml:id="A84622-001-a-1410">all</w>
      <pc xml:id="A84622-001-a-1420">:</pc>
     </l>
     <l xml:id="A84622-e250">
      <w lemma="accept" pos="vvb" xml:id="A84622-001-a-1430">Accept</w>
      <w lemma="eternal" pos="j" xml:id="A84622-001-a-1440">Eternal</w>
      <w lemma="honour" pos="n1" xml:id="A84622-001-a-1450">Honour</w>
      <pc xml:id="A84622-001-a-1460">,</pc>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-1470">and</w>
      <w lemma="that" pos="d" xml:id="A84622-001-a-1480">that</w>
      <w lemma="crown" pos="n1" xml:id="A84622-001-a-1490">Crown</w>
      <pc xml:id="A84622-001-a-1500">,</pc>
     </l>
     <l xml:id="A84622-e260">
      <w lemma="which" pos="crq" xml:id="A84622-001-a-1510">Which</w>
      <w lemma="virtue" pos="n1" reg="virtue" xml:id="A84622-001-a-1520">Vertue</w>
      <pc xml:id="A84622-001-a-1530">,</pc>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-1540">and</w>
      <w lemma="rare" pos="j" xml:id="A84622-001-a-1550">rare</w>
      <w lemma="action" pos="n2" xml:id="A84622-001-a-1560">Actions</w>
      <w lemma="make" pos="vvb" xml:id="A84622-001-a-1570">make</w>
      <w lemma="thou" pos="png" xml:id="A84622-001-a-1580">thine</w>
      <w lemma="own" pos="d" xml:id="A84622-001-a-1590">own</w>
      <pc unit="sentence" xml:id="A84622-001-a-1600">.</pc>
     </l>
     <l xml:id="A84622-e270">
      <w lemma="thou" pos="pns" xml:id="A84622-001-a-1610">Thou</w>
      <w lemma="shall" pos="vm2" xml:id="A84622-001-a-1620">shalt</w>
      <w lemma="eclipse" pos="vvi" xml:id="A84622-001-a-1630">Eclipse</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-1640">the</w>
      <w lemma="petty" pos="j" xml:id="A84622-001-a-1650">petty</w>
      <w lemma="court" pos="n2" xml:id="A84622-001-a-1660">Courts</w>
      <pc xml:id="A84622-001-a-1670">,</pc>
      <w lemma="where" pos="crq" xml:id="A84622-001-a-1680">where</w>
      <w lemma="thou" pos="pns" xml:id="A84622-001-a-1690">Thou</w>
      <pc xml:id="A84622-001-a-1700">,</pc>
     </l>
     <l xml:id="A84622-e280">
      <w lemma="too" pos="av" xml:id="A84622-001-a-1710">Too</w>
      <w lemma="long" pos="av-j" xml:id="A84622-001-a-1720">long</w>
      <w lemma="a" pos="d" xml:id="A84622-001-a-1730">a</w>
      <w lemma="noble" pos="j" xml:id="A84622-001-a-1740">Noble</w>
      <w lemma="sojourner" pos="n1" xml:id="A84622-001-a-1750">Sojourner</w>
      <pc xml:id="A84622-001-a-1760">,</pc>
      <w lemma="do" pos="vvd2" xml:id="A84622-001-a-1770">didst</w>
      <w lemma="bow" pos="vvi" xml:id="A84622-001-a-1780">bow</w>
      <pc unit="sentence" xml:id="A84622-001-a-1790">.</pc>
     </l>
     <l xml:id="A84622-e290">
      <w lemma="the" pos="d" xml:id="A84622-001-a-1800">The</w>
      <hi xml:id="A84622-e300">
       <w lemma="Monsieur" pos="ng1" xml:id="A84622-001-a-1810">Monsieur's</w>
      </hi>
      <w lemma="bravery" pos="n1" xml:id="A84622-001-a-1820">bravery</w>
      <w lemma="shall" pos="vmb" xml:id="A84622-001-a-1830">shall</w>
      <w lemma="veil" pos="vvi" reg="veil" xml:id="A84622-001-a-1840">vail</w>
      <w lemma="to" pos="acp" xml:id="A84622-001-a-1850">to</w>
      <w lemma="thou" pos="pno" xml:id="A84622-001-a-1860">Thee</w>
      <pc xml:id="A84622-001-a-1870">,</pc>
     </l>
     <l xml:id="A84622-e310">
      <w lemma="and" pos="cc" xml:id="A84622-001-a-1880">And</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-1890">the</w>
      <w lemma="grave" pos="j" xml:id="A84622-001-a-1900">grave</w>
      <hi xml:id="A84622-e320">
       <w lemma="don" pos="n1" xml:id="A84622-001-a-1910">Don</w>
      </hi>
      <w lemma="adore" pos="vvi" xml:id="A84622-001-a-1920">adore</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-1930">thy</w>
      <w lemma="majesty" pos="n1" reg="majesty" xml:id="A84622-001-a-1940">Majestie</w>
      <pc xml:id="A84622-001-a-1950">,</pc>
     </l>
     <l xml:id="A84622-e330">
      <w lemma="while" pos="cs" xml:id="A84622-001-a-1960">While</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-1970">thine</w>
      <w lemma="increase" pos="j-vg" reg="increasing" xml:id="A84622-001-a-1980">encreasing</w>
      <w lemma="glory" pos="n2" xml:id="A84622-001-a-1990">Glories</w>
      <w lemma="shall" pos="vmb" xml:id="A84622-001-a-2000">shall</w>
      <w lemma="outshine" pos="vvi" reg="outshine" xml:id="A84622-001-a-2010">out-shine</w>
     </l>
     <l xml:id="A84622-e340">
      <w lemma="the" pos="d" xml:id="A84622-001-a-2020">The</w>
      <hi xml:id="A84622-e350">
       <w lemma="plume" pos="n2" xml:id="A84622-001-a-2030">Plumes</w>
      </hi>
      <w join="right" lemma="of" pos="acp" xml:id="A84622-001-a-2040">o'</w>
      <w join="left" lemma="the" pos="d" xml:id="A84622-001-a-2041">th'</w>
      <hi xml:id="A84622-e360">
       <w lemma="one" pos="crd" xml:id="A84622-001-a-2050">One</w>
       <pc xml:id="A84622-001-a-2060">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-2070">and</w>
      <hi xml:id="A84622-e370">
       <w lemma="tother" pos="ng1" xml:id="A84622-001-a-2080">t'other's</w>
       <w lemma="golden" pos="j" xml:id="A84622-001-a-2090">Golden</w>
       <w lemma="mine" pos="n1" xml:id="A84622-001-a-2100">Mine</w>
       <pc unit="sentence" xml:id="A84622-001-a-2110">.</pc>
      </hi>
     </l>
     <l xml:id="A84622-e380">
      <w lemma="the" pos="d" xml:id="A84622-001-a-2120">The</w>
      <hi xml:id="A84622-e390">
       <w lemma="german" pos="jnn" xml:id="A84622-001-a-2130">German</w>
       <w lemma="eagle" pos="n1" xml:id="A84622-001-a-2140">Eagle</w>
       <pc xml:id="A84622-001-a-2150">,</pc>
      </hi>
      <w lemma="when" pos="crq" xml:id="A84622-001-a-2160">when</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-2170">thy</w>
      <hi xml:id="A84622-e400">
       <w lemma="lion" pos="n2" xml:id="A84622-001-a-2180">Lions</w>
      </hi>
      <w lemma="roar" pos="vvi" reg="roar" xml:id="A84622-001-a-2190">roare</w>
      <pc xml:id="A84622-001-a-2200">,</pc>
     </l>
     <l xml:id="A84622-e410">
      <w lemma="shall" pos="vmb" xml:id="A84622-001-a-2210">Shall</w>
      <w lemma="flag" pos="n1" xml:id="A84622-001-a-2220">flag</w>
      <w lemma="her" pos="po" xml:id="A84622-001-a-2230">her</w>
      <w lemma="wing" pos="n1" xml:id="A84622-001-a-2240">wing</w>
      <pc xml:id="A84622-001-a-2250">,</pc>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-2260">and</w>
      <w lemma="tower" pos="n1" reg="tower" xml:id="A84622-001-a-2270">towre</w>
      <w lemma="above" pos="acp" xml:id="A84622-001-a-2280">above</w>
      <w lemma="no" pos="dx" xml:id="A84622-001-a-2290">no</w>
      <w lemma="more" pos="dc" xml:id="A84622-001-a-2300">more</w>
      <pc xml:id="A84622-001-a-2310">;</pc>
     </l>
     <l xml:id="A84622-e420">
      <w lemma="shall" pos="vmb" xml:id="A84622-001-a-2320">Shall</w>
      <w lemma="gaze" pos="vvi" xml:id="A84622-001-a-2330">gaze</w>
      <w lemma="upon" pos="acp" xml:id="A84622-001-a-2340">upon</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-2350">Thy</w>
      <w lemma="lustre" pos="n1" xml:id="A84622-001-a-2360">Lustre</w>
      <pc xml:id="A84622-001-a-2370">,</pc>
      <w lemma="crouch" pos="vvb" xml:id="A84622-001-a-2380">crouch</w>
      <w lemma="down" pos="acp" xml:id="A84622-001-a-2390">down</w>
      <w lemma="low" pos="avc-j" xml:id="A84622-001-a-2400">lower</w>
      <pc xml:id="A84622-001-a-2410">,</pc>
     </l>
     <l xml:id="A84622-e430">
      <w lemma="and" pos="cc" xml:id="A84622-001-a-2420">And</w>
      <hi xml:id="A84622-e440">
       <w lemma="bask" pos="n1" xml:id="A84622-001-a-2430">bask</w>
      </hi>
      <w lemma="within" pos="acp" xml:id="A84622-001-a-2440">within</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-2450">the</w>
      <w lemma="sunshine" pos="n1" reg="sunshine" xml:id="A84622-001-a-2460">Sun-shine</w>
      <w lemma="of" pos="acp" xml:id="A84622-001-a-2470">of</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-2480">thy</w>
      <w lemma="power" pos="n1" xml:id="A84622-001-a-2490">Power</w>
      <pc xml:id="A84622-001-a-2500">:</pc>
     </l>
     <l xml:id="A84622-e450">
      <w lemma="as" pos="acp" xml:id="A84622-001-a-2510">As</w>
      <w lemma="for" pos="acp" xml:id="A84622-001-a-2520">for</w>
      <w lemma="those" pos="d" xml:id="A84622-001-a-2530">those</w>
      <w lemma="potentate" pos="n2" xml:id="A84622-001-a-2540">Potentates</w>
      <w lemma="that" pos="cs" xml:id="A84622-001-a-2550">that</w>
      <w lemma="lesser" pos="avc-j" xml:id="A84622-001-a-2560">lesser</w>
      <w lemma="be" pos="vvi" xml:id="A84622-001-a-2570">be</w>
      <pc xml:id="A84622-001-a-2580">,</pc>
     </l>
     <l xml:id="A84622-e460">
      <w lemma="they" pos="pns" xml:id="A84622-001-a-2590">They</w>
      <w lemma="shall" pos="vmb" xml:id="A84622-001-a-2600">shall</w>
      <w lemma="be" pos="vvi" xml:id="A84622-001-a-2610">be</w>
      <w lemma="great" pos="jc" xml:id="A84622-001-a-2620">Greater</w>
      <w lemma="if" pos="cs" xml:id="A84622-001-a-2630">if</w>
      <w lemma="they" pos="pns" xml:id="A84622-001-a-2640">they</w>
      <w lemma="stoop" pos="vvb" xml:id="A84622-001-a-2650">stoop</w>
      <w lemma="to" pos="acp" xml:id="A84622-001-a-2660">to</w>
      <w lemma="thou" pos="pno" xml:id="A84622-001-a-2670">Thee</w>
      <pc xml:id="A84622-001-a-2680">:</pc>
     </l>
     <l xml:id="A84622-e470">
      <w lemma="subject" pos="n2" xml:id="A84622-001-a-2690">Subjects</w>
      <w lemma="to" pos="acp" xml:id="A84622-001-a-2700">to</w>
      <hi xml:id="A84622-e480">
       <w lemma="such" pos="d" xml:id="A84622-001-a-2710">such</w>
      </hi>
      <w lemma="a" pos="d" xml:id="A84622-001-a-2720">a</w>
      <hi xml:id="A84622-e490">
       <w lemma="king" pos="n1" xml:id="A84622-001-a-2730">King</w>
       <pc xml:id="A84622-001-a-2740">,</pc>
      </hi>
      <w lemma="be" pos="vvb" xml:id="A84622-001-a-2750">are</w>
      <w lemma="better" pos="jc" xml:id="A84622-001-a-2760">better</w>
      <w lemma="far" pos="av-j" xml:id="A84622-001-a-2770">far</w>
      <pc xml:id="A84622-001-a-2780">,</pc>
     </l>
     <l xml:id="A84622-e500">
      <w lemma="and" pos="cc" xml:id="A84622-001-a-2790">And</w>
      <w lemma="happy" pos="jc" xml:id="A84622-001-a-2800">happier</w>
      <pc xml:id="A84622-001-a-2810">,</pc>
      <w lemma="than" pos="cs" xml:id="A84622-001-a-2820">than</w>
      <w lemma="other" pos="d" xml:id="A84622-001-a-2830">other</w>
      <hi xml:id="A84622-e510">
       <w lemma="monarch" pos="n2" xml:id="A84622-001-a-2840">Monarchs</w>
      </hi>
      <w lemma="be" pos="vvb" xml:id="A84622-001-a-2850">are</w>
      <pc unit="sentence" xml:id="A84622-001-a-2860">.</pc>
     </l>
    </lg>
    <lg xml:id="A84622-e520">
     <l xml:id="A84622-e530">
      <w lemma="heaven" pos="n1" reg="Heaven" xml:id="A84622-001-a-2870">Heav'n</w>
      <pc xml:id="A84622-001-a-2880">,</pc>
      <w lemma="and" pos="cc" xml:id="A84622-001-a-2890">and</w>
      <w lemma="brave" pos="j" xml:id="A84622-001-a-2900">brave</w>
      <hi xml:id="A84622-e540">
       <w lemma="monk" pos="nn1" reg="Monk" xml:id="A84622-001-a-2910">Monck</w>
       <pc xml:id="A84622-001-a-2920">,</pc>
      </hi>
      <w lemma="conspire" pos="vvb" xml:id="A84622-001-a-2930">conspire</w>
      <w lemma="to" pos="prt" xml:id="A84622-001-a-2940">to</w>
      <w lemma="make" pos="vvi" xml:id="A84622-001-a-2950">make</w>
      <w lemma="thy" pos="po" xml:id="A84622-001-a-2960">thy</w>
      <w lemma="reign" pos="n1" reg="reign" xml:id="A84622-001-a-2970">Raign</w>
     </l>
     <l xml:id="A84622-e550">
      <w lemma="transcend" pos="vvb" xml:id="A84622-001-a-2980">Transcend</w>
      <w lemma="the" pos="d" xml:id="A84622-001-a-2990">the</w>
      <w lemma="diadem" pos="n2" xml:id="A84622-001-a-3000">Diadems</w>
      <w lemma="of" pos="acp" xml:id="A84622-001-a-3010">of</w>
      <hi xml:id="A84622-e560">
       <w lemma="Charlemagne" pos="nn1" reg="Charlemagne" xml:id="A84622-001-a-3020">Charlemain</w>
       <pc unit="sentence" xml:id="A84622-001-a-3030">.</pc>
      </hi>
     </l>
    </lg>
    <closer xml:id="A84622-e570">
     <signed xml:id="A84622-e580">
      <w lemma="t." pos="ab" xml:id="A84622-001-a-3040">T.</w>
      <w lemma="f." pos="ab" xml:id="A84622-001-a-3050">F.</w>
      <pc unit="sentence" xml:id="A84622-001-a-3060"/>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A84622-e590">
   <div type="colophon" xml:id="A84622-e600">
    <p xml:id="A84622-e610">
     <hi xml:id="A84622-e620">
      <w lemma="LONDON" pos="nn1" xml:id="A84622-001-a-3070">LONDON</w>
      <pc xml:id="A84622-001-a-3080">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A84622-001-a-3090">Printed</w>
     <w lemma="for" pos="acp" xml:id="A84622-001-a-3100">for</w>
     <w lemma="HENRY" pos="nn1" xml:id="A84622-001-a-3110">HENRY</w>
     <w lemma="marsh" pos="nn1" xml:id="A84622-001-a-3120">MARSH</w>
     <w lemma="at" pos="acp" xml:id="A84622-001-a-3130">at</w>
     <w lemma="the" pos="d" xml:id="A84622-001-a-3140">the</w>
     <w lemma="prince" pos="ng1" reg="Prince's" xml:id="A84622-001-a-3150">Princes</w>
     <w lemma="arm" pos="n2" xml:id="A84622-001-a-3160">Arms</w>
     <w lemma="in" pos="acp" xml:id="A84622-001-a-3170">in</w>
     <hi xml:id="A84622-e630">
      <w lemma="chancery" pos="n1" xml:id="A84622-001-a-3180">Chancery</w>
      <w lemma="lane" pos="n1" xml:id="A84622-001-a-3190">Lane</w>
     </hi>
     <w lemma="near" pos="acp" xml:id="A84622-001-a-3200">near</w>
     <hi xml:id="A84622-e640">
      <w lemma="Fleetstreet" pos="nn1" xml:id="A84622-001-a-3210">Fleetstreet</w>
      <pc xml:id="A84622-001-a-3220">,</pc>
     </hi>
     <w lemma="mdclx" pos="crd" xml:id="A84622-001-a-3230">MDCLX</w>
     <pc unit="sentence" xml:id="A84622-001-a-3240">.</pc>
    </p>
   </div>
  </back>
 </text>
</TEI>
