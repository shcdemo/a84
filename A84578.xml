<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A84578">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>An act establishing the povvers of Lord Admiral of England, and Lord VVarden of the Cinque Ports, upon the Councel of State.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A84578 of text R212094 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.15[79]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A84578</idno>
    <idno type="STC">Wing E992</idno>
    <idno type="STC">Thomason 669.f.15[79]</idno>
    <idno type="STC">ESTC R212094</idno>
    <idno type="EEBO-CITATION">99870744</idno>
    <idno type="PROQUEST">99870744</idno>
    <idno type="VID">163153</idno>
    <idno type="PROQUESTGOID">2240952332</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A84578)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163153)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f15[79])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>An act establishing the povvers of Lord Admiral of England, and Lord VVarden of the Cinque Ports, upon the Councel of State.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by John Field, printer to the Parliament of England,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1650 [i.e. 1651]</date>
     </publicationStmt>
     <notesStmt>
      <note>Order to print dated: Die Jovis, 13 Februarii, 1650. Signed: Hen: Scobell, Cleric. Parliamenti.</note>
      <note>With Parliamentary seal at head of text.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History, Naval -- 17th century -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A84578</ep:tcp>
    <ep:estc> R212094</ep:estc>
    <ep:stc> (Thomason 669.f.15[79]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>An act establishing the povvers of Lord Admiral of England, and Lord VVarden of the Cinque Ports, upon the Councel of State.:</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1651</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>219</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A84578-e10">
  <body xml:id="A84578-e20">
   <pb facs="tcp:163153:1" rend="simple:additions" xml:id="A84578-001-a"/>
   <div type="act" xml:id="A84578-e30">
    <head xml:id="A84578-e40">
     <figure xml:id="A84578-e50">
      <figDesc xml:id="A84578-e60">blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A84578-e70">
     <w lemma="a" pos="d" xml:id="A84578-001-a-0010">AN</w>
     <w lemma="act" pos="n1" xml:id="A84578-001-a-0020">ACT</w>
     <w lemma="establish" pos="vvg" xml:id="A84578-001-a-0030">Establishing</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0040">the</w>
     <w lemma="power" pos="n2" reg="powers" xml:id="A84578-001-a-0050">POVVERS</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0060">OF</w>
     <w lemma="lord" pos="n1" xml:id="A84578-001-a-0070">Lord</w>
     <w lemma="admiral" pos="n1" xml:id="A84578-001-a-0080">Admiral</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0090">of</w>
     <hi xml:id="A84578-e80">
      <w lemma="England" pos="nn1" xml:id="A84578-001-a-0100">England</w>
      <pc xml:id="A84578-001-a-0110">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0120">and</w>
     <w lemma="lord" pos="n1" xml:id="A84578-001-a-0130">Lord</w>
     <w lemma="warden" pos="n1" reg="warden" xml:id="A84578-001-a-0140">VVarden</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0150">of</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0160">the</w>
     <w lemma="cinque ports" pos="nn2" xml:id="A84578-001-a-0170">Cinque Ports</w>
     <pc xml:id="A84578-001-a-0190">,</pc>
     <w lemma="upon" pos="acp" xml:id="A84578-001-a-0200">UPON</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0210">THE</w>
     <w lemma="council" pos="n1" reg="council" xml:id="A84578-001-a-0220">COUNCEL</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0230">of</w>
     <w lemma="state" pos="n1" xml:id="A84578-001-a-0240">STATE</w>
     <pc unit="sentence" xml:id="A84578-001-a-0250">.</pc>
    </head>
    <p xml:id="A84578-e90">
     <w lemma="be" pos="vvb" rend="decorinit" xml:id="A84578-001-a-0260">BE</w>
     <w lemma="it" pos="pn" xml:id="A84578-001-a-0270">it</w>
     <w lemma="enact" pos="vvn" xml:id="A84578-001-a-0280">Enacted</w>
     <w lemma="by" pos="acp" xml:id="A84578-001-a-0290">by</w>
     <w lemma="this" pos="d" xml:id="A84578-001-a-0300">this</w>
     <w lemma="present" pos="j" xml:id="A84578-001-a-0310">present</w>
     <w lemma="parliament" pos="n1" xml:id="A84578-001-a-0320">Parliament</w>
     <pc xml:id="A84578-001-a-0330">,</pc>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0340">and</w>
     <w lemma="by" pos="acp" xml:id="A84578-001-a-0350">by</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0360">the</w>
     <w lemma="authority" pos="n1" xml:id="A84578-001-a-0370">Authority</w>
     <w lemma="thereof" pos="av" xml:id="A84578-001-a-0380">thereof</w>
     <pc xml:id="A84578-001-a-0390">,</pc>
     <w lemma="that" pos="cs" xml:id="A84578-001-a-0400">That</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0410">the</w>
     <w lemma="council" pos="n1" reg="council" xml:id="A84578-001-a-0420">Councel</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0430">of</w>
     <w lemma="state" pos="n1" xml:id="A84578-001-a-0440">State</w>
     <w lemma="appoint" pos="vvn" xml:id="A84578-001-a-0450">appointed</w>
     <w lemma="by" pos="acp" xml:id="A84578-001-a-0460">by</w>
     <w lemma="authority" pos="n1" xml:id="A84578-001-a-0470">Authority</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0480">of</w>
     <w lemma="parliament" pos="n1" xml:id="A84578-001-a-0490">Parliament</w>
     <w lemma="for" pos="acp" xml:id="A84578-001-a-0500">for</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0510">the</w>
     <w lemma="year" pos="n1" xml:id="A84578-001-a-0520">year</w>
     <w lemma="ensue" pos="vvg" xml:id="A84578-001-a-0530">ensuing</w>
     <pc xml:id="A84578-001-a-0540">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A84578-001-a-0550">shall</w>
     <w lemma="have" pos="vvi" xml:id="A84578-001-a-0560">have</w>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0570">and</w>
     <w lemma="exercise" pos="vvi" xml:id="A84578-001-a-0580">exercise</w>
     <w lemma="all" pos="d" xml:id="A84578-001-a-0590">all</w>
     <w lemma="such" pos="d" xml:id="A84578-001-a-0600">such</w>
     <w lemma="power" pos="n1" xml:id="A84578-001-a-0610">Power</w>
     <pc xml:id="A84578-001-a-0620">,</pc>
     <w lemma="jurisdiction" pos="n1" reg="jurisdiction" xml:id="A84578-001-a-0630">Iurisdiction</w>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0640">and</w>
     <w lemma="authority" pos="n1" xml:id="A84578-001-a-0650">Authority</w>
     <pc xml:id="A84578-001-a-0660">,</pc>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0670">and</w>
     <w lemma="be" pos="vvb" xml:id="A84578-001-a-0680">are</w>
     <w lemma="hereby" pos="av" xml:id="A84578-001-a-0690">hereby</w>
     <w lemma="authorize" pos="vvn" xml:id="A84578-001-a-0700">authorized</w>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0710">and</w>
     <w lemma="enable" pos="vvn" xml:id="A84578-001-a-0720">enabled</w>
     <w lemma="to" pos="prt" xml:id="A84578-001-a-0730">to</w>
     <w lemma="do" pos="vvi" xml:id="A84578-001-a-0740">do</w>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0750">and</w>
     <w lemma="execute" pos="vvi" xml:id="A84578-001-a-0760">execute</w>
     <w lemma="all" pos="d" xml:id="A84578-001-a-0770">all</w>
     <w lemma="such" pos="d" xml:id="A84578-001-a-0780">such</w>
     <w lemma="thing" pos="n2" xml:id="A84578-001-a-0790">things</w>
     <w lemma="as" pos="acp" xml:id="A84578-001-a-0800">as</w>
     <w lemma="pertain" pos="vvi" xml:id="A84578-001-a-0810">pertain</w>
     <w lemma="to" pos="acp" xml:id="A84578-001-a-0820">to</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0830">the</w>
     <w lemma="office" pos="n1" xml:id="A84578-001-a-0840">Office</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0850">of</w>
     <w lemma="lord" pos="n1" xml:id="A84578-001-a-0860">Lord</w>
     <w lemma="admiral" pos="n1" xml:id="A84578-001-a-0870">Admiral</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0880">of</w>
     <hi xml:id="A84578-e100">
      <w lemma="England" pos="nn1" xml:id="A84578-001-a-0890">England</w>
      <pc xml:id="A84578-001-a-0900">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0910">and</w>
     <w lemma="lord" pos="n1" xml:id="A84578-001-a-0920">Lord</w>
     <w lemma="warden" pos="n1" xml:id="A84578-001-a-0930">Warden</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-0940">of</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-0950">the</w>
     <w lemma="cinque ports" pos="nn2" xml:id="A84578-001-a-0960">Cinque Ports</w>
     <pc xml:id="A84578-001-a-0980">,</pc>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-0990">and</w>
     <w lemma="all" pos="d" xml:id="A84578-001-a-1000">all</w>
     <w lemma="such" pos="d" xml:id="A84578-001-a-1010">such</w>
     <w lemma="other" pos="d" xml:id="A84578-001-a-1020">other</w>
     <w lemma="power" pos="n2" xml:id="A84578-001-a-1030">Powers</w>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-1040">and</w>
     <w lemma="authority" pos="n2" xml:id="A84578-001-a-1050">Authorities</w>
     <w lemma="as" pos="acp" xml:id="A84578-001-a-1060">as</w>
     <w lemma="be" pos="vvd" xml:id="A84578-001-a-1070">were</w>
     <w lemma="vest" pos="vvn" xml:id="A84578-001-a-1080">vested</w>
     <w lemma="in" pos="acp" xml:id="A84578-001-a-1090">in</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-1100">the</w>
     <w lemma="council" pos="n1" reg="council" xml:id="A84578-001-a-1110">Councel</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-1120">of</w>
     <w lemma="state" pos="n1" xml:id="A84578-001-a-1130">State</w>
     <w lemma="for" pos="acp" xml:id="A84578-001-a-1140">for</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-1150">the</w>
     <w lemma="year" pos="n1" xml:id="A84578-001-a-1160">year</w>
     <w lemma="past" pos="j" xml:id="A84578-001-a-1170">past</w>
     <pc xml:id="A84578-001-a-1180">,</pc>
     <w lemma="by" pos="acp" xml:id="A84578-001-a-1190">by</w>
     <w lemma="force" pos="n1" xml:id="A84578-001-a-1200">force</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-1210">of</w>
     <w lemma="a" pos="d" xml:id="A84578-001-a-1220">an</w>
     <w lemma="act" pos="n1" xml:id="A84578-001-a-1230">Act</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-1240">of</w>
     <w lemma="parliament" pos="n1" xml:id="A84578-001-a-1250">Parliament</w>
     <pc xml:id="A84578-001-a-1260">,</pc>
     <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A84578-001-a-1270">Entituled</w>
     <pc xml:id="A84578-001-a-1280">,</pc>
     <hi xml:id="A84578-e110">
      <w lemma="a" pos="d" xml:id="A84578-001-a-1290">An</w>
      <w lemma="act" pos="n1" xml:id="A84578-001-a-1300">Act</w>
      <w lemma="repeal" pos="n1-vg" xml:id="A84578-001-a-1310">Repealing</w>
      <w lemma="the" pos="d" xml:id="A84578-001-a-1320">the</w>
      <w lemma="power" pos="n2" xml:id="A84578-001-a-1330">Powers</w>
      <w lemma="former" pos="av-j" xml:id="A84578-001-a-1340">formerly</w>
      <w lemma="give" pos="vvn" xml:id="A84578-001-a-1350">given</w>
      <w lemma="to" pos="acp" xml:id="A84578-001-a-1360">to</w>
      <w lemma="the" pos="d" xml:id="A84578-001-a-1370">the</w>
      <w lemma="lord" pos="n1" xml:id="A84578-001-a-1380">Lord</w>
      <w lemma="admiral" pos="n1" xml:id="A84578-001-a-1390">Admiral</w>
      <pc xml:id="A84578-001-a-1400">,</pc>
      <w lemma="and" pos="cc" xml:id="A84578-001-a-1410">and</w>
      <w lemma="transfer" pos="vvg" xml:id="A84578-001-a-1420">transferring</w>
      <w lemma="it" pos="pn" xml:id="A84578-001-a-1430">it</w>
      <w lemma="to" pos="acp" xml:id="A84578-001-a-1440">to</w>
      <w lemma="the" pos="d" xml:id="A84578-001-a-1450">the</w>
      <w lemma="council" pos="n1" reg="council" xml:id="A84578-001-a-1460">Councel</w>
      <w lemma="of" pos="acp" xml:id="A84578-001-a-1470">of</w>
      <w lemma="state" pos="n1" xml:id="A84578-001-a-1480">State</w>
      <pc unit="sentence" xml:id="A84578-001-a-1490">.</pc>
     </hi>
     <w lemma="provide" pos="vvn" xml:id="A84578-001-a-1500">Provided</w>
     <pc xml:id="A84578-001-a-1510">,</pc>
     <w lemma="that" pos="cs" xml:id="A84578-001-a-1520">That</w>
     <w lemma="this" pos="d" xml:id="A84578-001-a-1530">this</w>
     <w lemma="act" pos="n1" xml:id="A84578-001-a-1540">Act</w>
     <w lemma="do" pos="vvb" xml:id="A84578-001-a-1550">do</w>
     <w lemma="continue" pos="vvi" xml:id="A84578-001-a-1560">continue</w>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-1570">and</w>
     <w lemma="stand" pos="vvi" xml:id="A84578-001-a-1580">stand</w>
     <w lemma="in" pos="acp" xml:id="A84578-001-a-1590">in</w>
     <w lemma="force" pos="n1" xml:id="A84578-001-a-1600">force</w>
     <w lemma="until" pos="acp" xml:id="A84578-001-a-1610">until</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-1620">the</w>
     <w lemma="first" pos="ord" xml:id="A84578-001-a-1630">First</w>
     <w lemma="day" pos="n1" xml:id="A84578-001-a-1640">day</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-1650">of</w>
     <hi xml:id="A84578-e120">
      <w lemma="July" pos="nn1" xml:id="A84578-001-a-1660">July</w>
      <pc xml:id="A84578-001-a-1670">,</pc>
     </hi>
     <w lemma="one" pos="crd" xml:id="A84578-001-a-1680">One</w>
     <w lemma="thousand" pos="crd" xml:id="A84578-001-a-1690">thousand</w>
     <w lemma="six" pos="crd" xml:id="A84578-001-a-1700">six</w>
     <w lemma="hundred" pos="crd" xml:id="A84578-001-a-1710">hundred</w>
     <w lemma="fifty" pos="crd" xml:id="A84578-001-a-1720">fifty</w>
     <w lemma="and" pos="cc" xml:id="A84578-001-a-1730">and</w>
     <w lemma="one" pos="crd" xml:id="A84578-001-a-1740">one</w>
     <pc unit="sentence" xml:id="A84578-001-a-1750">.</pc>
    </p>
    <div type="license" xml:id="A84578-e130">
     <opener xml:id="A84578-e140">
      <dateline xml:id="A84578-e150">
       <date xml:id="A84578-e160">
        <w lemma="die" pos="fla" xml:id="A84578-001-a-1760">Die</w>
        <w lemma="jovis" pos="fla" xml:id="A84578-001-a-1770">Jovis</w>
        <pc xml:id="A84578-001-a-1780">,</pc>
        <w lemma="13" pos="crd" xml:id="A84578-001-a-1790">13</w>
        <w lemma="februarii" pos="fla" xml:id="A84578-001-a-1800">Februarii</w>
        <pc xml:id="A84578-001-a-1810">,</pc>
        <w lemma="1650." pos="crd" xml:id="A84578-001-a-1820">1650.</w>
        <pc unit="sentence" xml:id="A84578-001-a-1830"/>
       </date>
      </dateline>
     </opener>
     <p xml:id="A84578-e170">
      <w lemma="order" pos="j-vn" xml:id="A84578-001-a-1840">ORdered</w>
      <w lemma="by" pos="acp" xml:id="A84578-001-a-1850">by</w>
      <w lemma="the" pos="d" xml:id="A84578-001-a-1860">the</w>
      <w lemma="parliament" pos="n1" xml:id="A84578-001-a-1870">Parliament</w>
      <pc xml:id="A84578-001-a-1880">,</pc>
      <w lemma="that" pos="cs" xml:id="A84578-001-a-1890">That</w>
      <w lemma="this" pos="d" xml:id="A84578-001-a-1900">this</w>
      <w lemma="act" pos="n1" xml:id="A84578-001-a-1910">Act</w>
      <w lemma="be" pos="vvb" xml:id="A84578-001-a-1920">be</w>
      <w lemma="forthwith" pos="av" xml:id="A84578-001-a-1930">forthwith</w>
      <w lemma="print" pos="vvn" xml:id="A84578-001-a-1940">printed</w>
      <w lemma="and" pos="cc" xml:id="A84578-001-a-1950">and</w>
      <w lemma="publish" pos="vvn" xml:id="A84578-001-a-1960">published</w>
      <pc unit="sentence" xml:id="A84578-001-a-1970">.</pc>
     </p>
     <closer xml:id="A84578-e180">
      <signed xml:id="A84578-e190">
       <w lemma="hen" pos="ab" xml:id="A84578-001-a-1980">Hen:</w>
       <w lemma="Scobell" pos="nn1" xml:id="A84578-001-a-2000">Scobell</w>
       <pc xml:id="A84578-001-a-2010">,</pc>
       <w lemma="cleric" pos="n1" xml:id="A84578-001-a-2020">Cleric</w>
       <pc unit="sentence" xml:id="A84578-001-a-2030">.</pc>
       <w lemma="parliamenti" pos="fla" xml:id="A84578-001-a-2040">Parliamenti</w>
       <pc unit="sentence" xml:id="A84578-001-a-2050">.</pc>
      </signed>
     </closer>
    </div>
   </div>
  </body>
  <back xml:id="A84578-e200">
   <div type="colophon" xml:id="A84578-e210">
    <p xml:id="A84578-e220">
     <hi xml:id="A84578-e230">
      <w lemma="london" pos="nn1" xml:id="A84578-001-a-2060">London</w>
      <pc xml:id="A84578-001-a-2070">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A84578-001-a-2080">Printed</w>
     <w lemma="by" pos="acp" xml:id="A84578-001-a-2090">by</w>
     <hi xml:id="A84578-e240">
      <w lemma="John" pos="nn1" xml:id="A84578-001-a-2100">John</w>
      <w lemma="field" pos="nn1" xml:id="A84578-001-a-2110">Field</w>
      <pc xml:id="A84578-001-a-2120">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A84578-001-a-2130">Printer</w>
     <w lemma="to" pos="acp" xml:id="A84578-001-a-2140">to</w>
     <w lemma="the" pos="d" xml:id="A84578-001-a-2150">the</w>
     <w lemma="parliament" pos="n1" xml:id="A84578-001-a-2160">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A84578-001-a-2170">of</w>
     <hi xml:id="A84578-e250">
      <w lemma="England" pos="nn1" xml:id="A84578-001-a-2180">England</w>
      <pc xml:id="A84578-001-a-2190">,</pc>
     </hi>
     <w lemma="1650." pos="crd" xml:id="A84578-001-a-2200">1650.</w>
     <pc unit="sentence" xml:id="A84578-001-a-2210"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
