<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A84108">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A letter sent from His Excellency, Robert Earle of Essex, &amp;c. to the Lord Maior of London.</title>
    <author>Essex, Robert Devereux, Earl of, 1591-1646.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A84108 of text R210999 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.5[77]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A84108</idno>
    <idno type="STC">Wing E3321</idno>
    <idno type="STC">Thomason 669.f.5[77]</idno>
    <idno type="STC">ESTC R210999</idno>
    <idno type="EEBO-CITATION">99869740</idno>
    <idno type="PROQUEST">99869740</idno>
    <idno type="VID">160790</idno>
    <idno type="PROQUESTGOID">2240944723</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A84108)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160790)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f5[77])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A letter sent from His Excellency, Robert Earle of Essex, &amp;c. to the Lord Maior of London.</title>
      <author>Essex, Robert Devereux, Earl of, 1591-1646.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Septemb. 19. 1642. London, printed for William Gay,</publisher>
      <pubPlace>[London] :</pubPlace>
      <date>[1642]</date>
     </publicationStmt>
     <notesStmt>
      <note>Requesting a loan of £100,000 for the use of the army. -- Thomason Catalogue.</note>
      <note>With engraved border.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Finance -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A84108</ep:tcp>
    <ep:estc> R210999</ep:estc>
    <ep:stc> (Thomason 669.f.5[77]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>A letter sent from his Excellency, Robert Earle of Essex, &amp;c. to the Lord Maior of London.</ep:title>
    <ep:author>Essex, Robert Devereux, Earl of</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>390</ep:wordCount>
    <ep:defectiveTokenCount>3</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>77</ep:defectRate>
    <ep:finalGrade>D</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 77 defects per 10,000 words puts this text in the D category of texts with between 35 and 100 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A84108-e10">
  <body xml:id="A84108-e20">
   <pb facs="tcp:160790:1" rend="simple:additions" xml:id="A84108-001-a"/>
   <div type="letter" xml:id="A84108-e30">
    <head xml:id="A84108-e40">
     <w lemma="a" pos="d" xml:id="A84108-001-a-0010">A</w>
     <w lemma="letter" pos="n1" xml:id="A84108-001-a-0020">LETTER</w>
     <w lemma="send" pos="vvn" xml:id="A84108-001-a-0030">Sent</w>
     <w lemma="from" pos="acp" xml:id="A84108-001-a-0040">from</w>
     <w lemma="his" pos="po" xml:id="A84108-001-a-0050">his</w>
     <w lemma="excellency" pos="n1" xml:id="A84108-001-a-0060">Excellency</w>
     <pc xml:id="A84108-001-a-0070">,</pc>
     <w lemma="ROBERT" pos="nn1" xml:id="A84108-001-a-0080">ROBERT</w>
     <w lemma="earl" pos="n1" reg="Earl" xml:id="A84108-001-a-0090">Earle</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0100">of</w>
     <hi xml:id="A84108-e50">
      <w lemma="ESSEX" pos="nn1" xml:id="A84108-001-a-0110">ESSEX</w>
      <pc xml:id="A84108-001-a-0120">,</pc>
     </hi>
     <w lemma="etc." pos="ab" xml:id="A84108-001-a-0130">&amp;c.</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-0140">to</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-0150">the</w>
     <w lemma="lord" pos="n1" xml:id="A84108-001-a-0160">Lord</w>
     <w lemma="MADOR" pos="nn1" xml:id="A84108-001-a-0170">MADOR</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0180">of</w>
     <hi xml:id="A84108-e60">
      <w lemma="London" pos="nn1" xml:id="A84108-001-a-0190">London</w>
      <pc unit="sentence" xml:id="A84108-001-a-0200">.</pc>
     </hi>
    </head>
    <opener xml:id="A84108-e70">
     <salute xml:id="A84108-e80">
      <w lemma="my" pos="po" xml:id="A84108-001-a-0210">My</w>
      <w lemma="lord" pos="n1" xml:id="A84108-001-a-0220">LORD</w>
      <w lemma="and" pos="cc" xml:id="A84108-001-a-0230">and</w>
      <w lemma="gentleman" pos="n2" xml:id="A84108-001-a-0240">Gentlemen</w>
      <pc xml:id="A84108-001-a-0250">,</pc>
     </salute>
    </opener>
    <p xml:id="A84108-e90">
     <w lemma="i" pos="pns" xml:id="A84108-001-a-0260">I</w>
     <w lemma="receive" pos="vvd" xml:id="A84108-001-a-0270">Received</w>
     <w lemma="so" pos="av" xml:id="A84108-001-a-0280">so</w>
     <w lemma="great" pos="j" xml:id="A84108-001-a-0290">great</w>
     <w lemma="expression" pos="n2" xml:id="A84108-001-a-0300">expressions</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0310">of</w>
     <w lemma="affection" pos="n2" xml:id="A84108-001-a-0320">affections</w>
     <w lemma="both" pos="av-d" xml:id="A84108-001-a-0330">both</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-0340">to</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-0350">the</w>
     <w lemma="cause" pos="n1" xml:id="A84108-001-a-0360">Cause</w>
     <pc xml:id="A84108-001-a-0370">,</pc>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-0380">and</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-0390">to</w>
     <w lemma="myself" pos="pr" reg="myself" xml:id="A84108-001-a-0400">my selfe</w>
     <pc xml:id="A84108-001-a-0420">,</pc>
     <w lemma="from" pos="acp" xml:id="A84108-001-a-0430">from</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-0440">the</w>
     <w lemma="city" pos="n1" xml:id="A84108-001-a-0450">City</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0460">of</w>
     <hi xml:id="A84108-e100">
      <w lemma="London" pos="nn1" xml:id="A84108-001-a-0470">London</w>
      <pc xml:id="A84108-001-a-0480">,</pc>
     </hi>
     <w lemma="at" pos="acp" xml:id="A84108-001-a-0490">at</w>
     <w lemma="my" pos="po" xml:id="A84108-001-a-0500">my</w>
     <w lemma="departure" pos="n1" xml:id="A84108-001-a-0510">departure</w>
     <w lemma="from" pos="acp" xml:id="A84108-001-a-0520">from</w>
     <w lemma="you" pos="pn" xml:id="A84108-001-a-0530">you</w>
     <pc xml:id="A84108-001-a-0540">,</pc>
     <w lemma="that" pos="cs" xml:id="A84108-001-a-0550">that</w>
     <w lemma="i" pos="pns" xml:id="A84108-001-a-0560">I</w>
     <w lemma="can" pos="vmbx" xml:id="A84108-001-a-0570">cannot</w>
     <w lemma="despair" pos="vvi" reg="despair" xml:id="A84108-001-a-0580">dispaire</w>
     <pc xml:id="A84108-001-a-0590">,</pc>
     <w lemma="but" pos="acp" xml:id="A84108-001-a-0600">but</w>
     <w lemma="to" pos="prt" xml:id="A84108-001-a-0610">to</w>
     <w lemma="obtain" pos="vvi" reg="obtain" xml:id="A84108-001-a-0620">obtaine</w>
     <w lemma="my" pos="po" xml:id="A84108-001-a-0630">my</w>
     <w lemma="suit" pos="n1" xml:id="A84108-001-a-0640">suit</w>
     <w lemma="from" pos="acp" xml:id="A84108-001-a-0650">from</w>
     <w lemma="you" pos="pn" xml:id="A84108-001-a-0660">you</w>
     <pc xml:id="A84108-001-a-0670">,</pc>
     <w lemma="that" pos="cs" xml:id="A84108-001-a-0680">that</w>
     <w lemma="shall" pos="vmb" xml:id="A84108-001-a-0690">shall</w>
     <w lemma="be" pos="vvi" xml:id="A84108-001-a-0700">be</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-0710">an</w>
     <w lemma="advantage" pos="n1" xml:id="A84108-001-a-0720">advantage</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-0730">to</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-0740">the</w>
     <w lemma="common" pos="j" xml:id="A84108-001-a-0750">Common</w>
     <w lemma="wealth" pos="n1" xml:id="A84108-001-a-0760">wealth</w>
     <pc xml:id="A84108-001-a-0770">;</pc>
     <w lemma="upon" pos="acp" xml:id="A84108-001-a-0780">upon</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-0790">a</w>
     <w lemma="true" pos="j" xml:id="A84108-001-a-0800">true</w>
     <w lemma="judgement" pos="n1" xml:id="A84108-001-a-0810">judgement</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0820">of</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-0830">the</w>
     <w lemma="condition" pos="n1" xml:id="A84108-001-a-0840">condition</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0850">of</w>
     <w lemma="our" pos="po" xml:id="A84108-001-a-0860">our</w>
     <w lemma="affair" pos="n2" reg="affairs" xml:id="A84108-001-a-0870">affaires</w>
     <pc xml:id="A84108-001-a-0880">,</pc>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-0890">and</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0900">of</w>
     <w lemma="that" pos="d" xml:id="A84108-001-a-0910">that</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-0920">of</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-0930">the</w>
     <w lemma="enemy" pos="n1" xml:id="A84108-001-a-0940">Enemy</w>
     <pc xml:id="A84108-001-a-0950">;</pc>
     <w lemma="i" pos="pns" xml:id="A84108-001-a-0960">I</w>
     <w lemma="be" pos="vvm" xml:id="A84108-001-a-0970">am</w>
     <w lemma="confident" pos="j" xml:id="A84108-001-a-0980">confident</w>
     <w lemma="that" pos="cs" xml:id="A84108-001-a-0990">that</w>
     <w lemma="we" pos="pns" xml:id="A84108-001-a-1000">we</w>
     <w lemma="may" pos="vmb" xml:id="A84108-001-a-1010">may</w>
     <w lemma="bring" pos="vvi" xml:id="A84108-001-a-1020">bring</w>
     <w lemma="this" pos="d" xml:id="A84108-001-a-1030">this</w>
     <w lemma="business" pos="n1" reg="business" xml:id="A84108-001-a-1040">businesse</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-1050">to</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-1060">a</w>
     <w lemma="quick" pos="j" reg="quick" xml:id="A84108-001-a-1070">quicke</w>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-1080">and</w>
     <w lemma="happy" pos="j" xml:id="A84108-001-a-1090">happy</w>
     <w lemma="conclusion" pos="n1" xml:id="A84108-001-a-1100">conclusion</w>
     <pc xml:id="A84108-001-a-1110">,</pc>
     <w lemma="God" pos="nn1" xml:id="A84108-001-a-1120">God</w>
     <w lemma="do" pos="vvz" xml:id="A84108-001-a-1130">doth</w>
     <w lemma="bless" pos="vvi" reg="bless" xml:id="A84108-001-a-1140">blesse</w>
     <w lemma="we" pos="pno" xml:id="A84108-001-a-1150">us</w>
     <w lemma="with" pos="acp" xml:id="A84108-001-a-1160">with</w>
     <w lemma="so" pos="av" xml:id="A84108-001-a-1170">so</w>
     <w lemma="good" pos="j" xml:id="A84108-001-a-1180">good</w>
     <w lemma="success" pos="n1" reg="success" xml:id="A84108-001-a-1190">successe</w>
     <w lemma="daily" pos="av-j" xml:id="A84108-001-a-1200">daily</w>
     <pc xml:id="A84108-001-a-1210">;</pc>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-1220">and</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-1230">the</w>
     <w lemma="other" pos="d" xml:id="A84108-001-a-1240">other</w>
     <w lemma="part" pos="n1" xml:id="A84108-001-a-1250">part</w>
     <w lemma="by" pos="acp" xml:id="A84108-001-a-1260">by</w>
     <w lemma="their" pos="po" xml:id="A84108-001-a-1270">their</w>
     <w lemma="plunder" pos="vvg" xml:id="A84108-001-a-1280">plundering</w>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-1290">and</w>
     <w lemma="burn" pos="vvg" xml:id="A84108-001-a-1300">burning</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-1310">of</w>
     <w lemma="town" pos="n2" reg="towns" xml:id="A84108-001-a-1320">Townes</w>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-1330">and</w>
     <w lemma="house" pos="n2" xml:id="A84108-001-a-1340">Houses</w>
     <pc xml:id="A84108-001-a-1350">,</pc>
     <w lemma="grow" pos="vvb" xml:id="A84108-001-a-1360">grow</w>
     <w lemma="so" pos="av" xml:id="A84108-001-a-1370">so</w>
     <w lemma="odious" pos="j" xml:id="A84108-001-a-1380">odious</w>
     <w lemma="that" pos="cs" xml:id="A84108-001-a-1390">that</w>
     <w lemma="they" pos="pns" xml:id="A84108-001-a-1400">they</w>
     <w lemma="grow" pos="vvb" xml:id="A84108-001-a-1410">grow</w>
     <w lemma="weak" pos="jc" xml:id="A84108-001-a-1420">weaker</w>
     <pc xml:id="A84108-001-a-1430">▪</pc>
     <w lemma="we" pos="pns" reg="we" xml:id="A84108-001-a-1440">wee</w>
     <w lemma="strong" pos="jc" xml:id="A84108-001-a-1450">stronger</w>
     <w lemma="everywhere" pos="av" xml:id="A84108-001-a-1460">everywhere</w>
     <pc xml:id="A84108-001-a-1470">;</pc>
     <w lemma="yet" pos="av" xml:id="A84108-001-a-1480">yet</w>
     <w lemma="be" pos="vvb" xml:id="A84108-001-a-1490">are</w>
     <w lemma="we" pos="pns" xml:id="A84108-001-a-1500">we</w>
     <w lemma="in" pos="acp" xml:id="A84108-001-a-1510">in</w>
     <w lemma="one" pos="crd" xml:id="A84108-001-a-1520">one</w>
     <w lemma="great" pos="j" xml:id="A84108-001-a-1530">great</w>
     <w lemma="straight" pos="av-j" xml:id="A84108-001-a-1540">straight</w>
     <pc xml:id="A84108-001-a-1550">,</pc>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-1560">and</w>
     <w lemma="such" pos="d" xml:id="A84108-001-a-1570">such</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-1580">a</w>
     <w lemma="one" pos="pi" xml:id="A84108-001-a-1590">one</w>
     <pc xml:id="A84108-001-a-1600">,</pc>
     <w lemma="as" pos="acp" xml:id="A84108-001-a-1610">as</w>
     <w lemma="if" pos="cs" xml:id="A84108-001-a-1620">if</w>
     <w lemma="it" pos="pn" xml:id="A84108-001-a-1630">it</w>
     <w lemma="be" pos="vvb" xml:id="A84108-001-a-1640">be</w>
     <w lemma="not" pos="xx" xml:id="A84108-001-a-1650">not</w>
     <w lemma="speedy" pos="av-j" xml:id="A84108-001-a-1660">speedily</w>
     <w lemma="remedy" pos="vvn" xml:id="A84108-001-a-1670">remedied</w>
     <pc xml:id="A84108-001-a-1680">,</pc>
     <w lemma="may" pos="vmb" xml:id="A84108-001-a-1690">may</w>
     <w lemma="quash" pos="vvi" xml:id="A84108-001-a-1700">quash</w>
     <w lemma="all" pos="d" xml:id="A84108-001-a-1710">all</w>
     <w lemma="our" pos="po" xml:id="A84108-001-a-1720">our</w>
     <w lemma="hope" pos="n2" xml:id="A84108-001-a-1730">hopes</w>
     <pc xml:id="A84108-001-a-1740">,</pc>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-1750">and</w>
     <w lemma="endanger" pos="vvi" xml:id="A84108-001-a-1760">endanger</w>
     <w lemma="that" pos="d" xml:id="A84108-001-a-1770">that</w>
     <w lemma="peace" pos="n1" xml:id="A84108-001-a-1780">peace</w>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-1790">and</w>
     <w lemma="liberty" pos="n1" reg="liberty" xml:id="A84108-001-a-1800">libertie</w>
     <w lemma="which" pos="crq" xml:id="A84108-001-a-1810">which</w>
     <w lemma="we" pos="pns" xml:id="A84108-001-a-1820">we</w>
     <w lemma="so" pos="av" xml:id="A84108-001-a-1830">so</w>
     <w lemma="much" pos="av-d" xml:id="A84108-001-a-1840">much</w>
     <w lemma="labour" pos="n1" xml:id="A84108-001-a-1850">labour</w>
     <w lemma="for" pos="acp" xml:id="A84108-001-a-1860">for</w>
     <pc xml:id="A84108-001-a-1870">;</pc>
     <w lemma="our" pos="po" xml:id="A84108-001-a-1880">our</w>
     <w lemma="treasure" pos="n1" xml:id="A84108-001-a-1890">treasure</w>
     <pc xml:id="A84108-001-a-1900">,</pc>
     <w lemma="which" pos="crq" xml:id="A84108-001-a-1910">which</w>
     <w lemma="must" pos="vmb" xml:id="A84108-001-a-1920">must</w>
     <w lemma="maintain" pos="vvi" reg="maintain" xml:id="A84108-001-a-1930">maintaine</w>
     <w lemma="our" pos="po" xml:id="A84108-001-a-1940">our</w>
     <w lemma="army" pos="n1" xml:id="A84108-001-a-1950">Army</w>
     <pc xml:id="A84108-001-a-1960">▪</pc>
     <w lemma="grow" pos="vvz" reg="grows" xml:id="A84108-001-a-1970">growes</w>
     <w lemma="near" pos="acp" reg="near" xml:id="A84108-001-a-1980">neere</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-1990">an</w>
     <w lemma="end" pos="n1" xml:id="A84108-001-a-2000">end</w>
     <pc xml:id="A84108-001-a-2010">;</pc>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-2020">and</w>
     <w lemma="you" pos="pn" xml:id="A84108-001-a-2030">you</w>
     <w lemma="well" pos="av" xml:id="A84108-001-a-2040">well</w>
     <w lemma="know" pos="vvb" xml:id="A84108-001-a-2050">know</w>
     <w lemma="our" pos="po" xml:id="A84108-001-a-2060">our</w>
     <w lemma="army" pos="n1" xml:id="A84108-001-a-2070">Army</w>
     <w lemma="consist" pos="vvz" xml:id="A84108-001-a-2080">consists</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-2090">of</w>
     <w lemma="such" pos="d" xml:id="A84108-001-a-2100">such</w>
     <w lemma="as" pos="acp" xml:id="A84108-001-a-2110">as</w>
     <w lemma="can" pos="vmbx" xml:id="A84108-001-a-2120">cannot</w>
     <w lemma="be" pos="vvi" xml:id="A84108-001-a-2130">be</w>
     <w lemma="keep" pos="vvn" xml:id="A84108-001-a-2140">kept</w>
     <w lemma="one" pos="crd" xml:id="A84108-001-a-2150">one</w>
     <w lemma="day" pos="n1" xml:id="A84108-001-a-2160">day</w>
     <w lemma="together" pos="av" xml:id="A84108-001-a-2170">together</w>
     <w lemma="without" pos="acp" xml:id="A84108-001-a-2180">without</w>
     <w lemma="pay" pos="n1" xml:id="A84108-001-a-2190">pay</w>
     <pc xml:id="A84108-001-a-2200">:</pc>
     <w lemma="what" pos="crq" xml:id="A84108-001-a-2210">what</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-2220">a</w>
     <w lemma="ruin" pos="n1" reg="ruin" xml:id="A84108-001-a-2230">ruine</w>
     <w lemma="it" pos="pn" xml:id="A84108-001-a-2240">it</w>
     <w lemma="will" pos="vmd" xml:id="A84108-001-a-2250">would</w>
     <w lemma="bring" pos="vvi" xml:id="A84108-001-a-2260">bring</w>
     <w lemma="upon" pos="acp" xml:id="A84108-001-a-2270">upon</w>
     <w lemma="we" pos="pno" xml:id="A84108-001-a-2280">us</w>
     <w lemma="all" pos="d" xml:id="A84108-001-a-2290">all</w>
     <pc xml:id="A84108-001-a-2300">,</pc>
     <w lemma="if" pos="cs" xml:id="A84108-001-a-2310">if</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-2320">a</w>
     <w lemma="disband" pos="n1-vg" xml:id="A84108-001-a-2330">disbanding</w>
     <w lemma="shall" pos="vmd" xml:id="A84108-001-a-2340">should</w>
     <w lemma="happen" pos="vvi" xml:id="A84108-001-a-2350">happen</w>
     <pc xml:id="A84108-001-a-2360">,</pc>
     <w lemma="i" pos="pns" xml:id="A84108-001-a-2370">I</w>
     <w lemma="leave" pos="vvb" xml:id="A84108-001-a-2380">leave</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-2390">to</w>
     <w lemma="your" pos="po" xml:id="A84108-001-a-2400">your</w>
     <w lemma="judgement" pos="n2" xml:id="A84108-001-a-2410">judgements</w>
     <pc xml:id="A84108-001-a-2420">:</pc>
     <w lemma="my" pos="po" xml:id="A84108-001-a-2430">My</w>
     <w lemma="desire" pos="n1" xml:id="A84108-001-a-2440">desire</w>
     <w lemma="unto" pos="acp" xml:id="A84108-001-a-2450">unto</w>
     <w lemma="you" pos="pn" xml:id="A84108-001-a-2460">you</w>
     <w lemma="be" pos="vvz" xml:id="A84108-001-a-2470">is</w>
     <pc xml:id="A84108-001-a-2480">,</pc>
     <w lemma="that" pos="cs" xml:id="A84108-001-a-2490">that</w>
     <w lemma="you" pos="pn" xml:id="A84108-001-a-2500">you</w>
     <w lemma="will" pos="vmd" xml:id="A84108-001-a-2510">would</w>
     <w lemma="supply" pos="vvi" xml:id="A84108-001-a-2520">supply</w>
     <w lemma="we" pos="pno" xml:id="A84108-001-a-2530">us</w>
     <w lemma="with" pos="acp" xml:id="A84108-001-a-2540">with</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-2550">a</w>
     <w lemma="speedy" pos="j" xml:id="A84108-001-a-2560">speedy</w>
     <w lemma="loan" pos="n1" reg="loan" xml:id="A84108-001-a-2570">loane</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-2580">of</w>
     <w lemma="one" pos="crd" xml:id="A84108-001-a-2590">one</w>
     <w lemma="hundred" pos="crd" xml:id="A84108-001-a-2600">hundred</w>
     <w lemma="thousand" pos="crd" xml:id="A84108-001-a-2610">thousand</w>
     <w lemma="pound" pos="n2" xml:id="A84108-001-a-2620">pounds</w>
     <pc xml:id="A84108-001-a-2630">,</pc>
     <w lemma="which" pos="crq" xml:id="A84108-001-a-2640">which</w>
     <w lemma="i" pos="pns" xml:id="A84108-001-a-2650">I</w>
     <w lemma="be" pos="vvm" xml:id="A84108-001-a-2660">am</w>
     <w lemma="confident" pos="j" xml:id="A84108-001-a-2670">confident</w>
     <w lemma="will" pos="vmd" xml:id="A84108-001-a-2680">would</w>
     <pc join="right" xml:id="A84108-001-a-2690">(</pc>
     <w lemma="with" pos="acp" xml:id="A84108-001-a-2700">with</w>
     <w lemma="God" pos="nng1" reg="God's" xml:id="A84108-001-a-2710">Gods</w>
     <w lemma="blessing" pos="n1" xml:id="A84108-001-a-2720">blessing</w>
     <pc xml:id="A84108-001-a-2730">)</pc>
     <w lemma="bring" pos="vvb" xml:id="A84108-001-a-2740">bring</w>
     <w lemma="these" pos="d" xml:id="A84108-001-a-2750">these</w>
     <w lemma="unhappy" pos="j" xml:id="A84108-001-a-2760">unhappy</w>
     <w lemma="distraction" pos="n2" xml:id="A84108-001-a-2770">distractions</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-2780">to</w>
     <w lemma="a" pos="d" xml:id="A84108-001-a-2790">an</w>
     <w lemma="end" pos="n1" xml:id="A84108-001-a-2800">end</w>
     <w lemma="quick" pos="av-j" xml:id="A84108-001-a-2810">quickly</w>
     <pc xml:id="A84108-001-a-2820">:</pc>
     <w lemma="your" pos="po" xml:id="A84108-001-a-2830">your</w>
     <w lemma="city" pos="n1" reg="city" xml:id="A84108-001-a-2840">Citie</w>
     <w lemma="have" pos="vvz" xml:id="A84108-001-a-2850">hath</w>
     <w lemma="hitherto" pos="av" xml:id="A84108-001-a-2860">hitherto</w>
     <w lemma="have" pos="vvd" xml:id="A84108-001-a-2870">had</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-2880">the</w>
     <w lemma="honour" pos="n1" xml:id="A84108-001-a-2890">honour</w>
     <pc xml:id="A84108-001-a-2900">,</pc>
     <w lemma="next" pos="ord" xml:id="A84108-001-a-2910">next</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-2920">to</w>
     <w lemma="God" pos="nn1" xml:id="A84108-001-a-2930">God</w>
     <pc xml:id="A84108-001-a-2940">,</pc>
     <w lemma="to" pos="prt" xml:id="A84108-001-a-2950">to</w>
     <w lemma="be" pos="vvi" xml:id="A84108-001-a-2960">be</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-2970">the</w>
     <w lemma="chief" pos="js" xml:id="A84108-001-a-2980">chiefest</w>
     <w lemma="safety" pos="n1" reg="safety" xml:id="A84108-001-a-2990">saftie</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-3000">of</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-3010">the</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A84108-001-a-3020">Kingdome</w>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-3030">and</w>
     <w lemma="parliament" pos="n1" xml:id="A84108-001-a-3040">Parliament</w>
     <pc xml:id="A84108-001-a-3050">;</pc>
     <w lemma="this" pos="d" xml:id="A84108-001-a-3060">this</w>
     <w lemma="will" pos="vmb" xml:id="A84108-001-a-3070">will</w>
     <w lemma="render" pos="vvi" xml:id="A84108-001-a-3080">render</w>
     <w lemma="you" pos="pn" xml:id="A84108-001-a-3090">you</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-3100">to</w>
     <w lemma="all" pos="d" xml:id="A84108-001-a-3110">all</w>
     <w lemma="posterity" pos="n1" reg="posterity" xml:id="A84108-001-a-3120">posteritie</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-3130">the</w>
     <w lemma="finisher" pos="n2" xml:id="A84108-001-a-3140">finishers</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-3150">of</w>
     <w lemma="this" pos="d" xml:id="A84108-001-a-3160">this</w>
     <w lemma="great" pos="j" xml:id="A84108-001-a-3170">great</w>
     <w lemma="work" pos="n1" reg="work" xml:id="A84108-001-a-3180">worke</w>
     <pc unit="sentence" xml:id="A84108-001-a-3190">.</pc>
     <w lemma="if" pos="cs" xml:id="A84108-001-a-3200">If</w>
     <w lemma="any" pos="d" xml:id="A84108-001-a-3210">any</w>
     <w lemma="thing" pos="n1" xml:id="A84108-001-a-3220">thing</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-3230">of</w>
     <w lemma="particular" pos="j" xml:id="A84108-001-a-3240">particular</w>
     <w lemma="love" pos="n1" xml:id="A84108-001-a-3250">love</w>
     <w lemma="or" pos="cc" xml:id="A84108-001-a-3260">or</w>
     <w lemma="respect" pos="n1" xml:id="A84108-001-a-3270">respect</w>
     <w lemma="to" pos="acp" xml:id="A84108-001-a-3280">to</w>
     <w lemma="i" pos="pno" xml:id="A84108-001-a-3290">me</w>
     <w lemma="may" pos="vmb" xml:id="A84108-001-a-3300">may</w>
     <w lemma="be" pos="vvi" xml:id="A84108-001-a-3310">be</w>
     <w lemma="any" pos="d" xml:id="A84108-001-a-3320">any</w>
     <w lemma="argument" pos="n1" xml:id="A84108-001-a-3330">argument</w>
     <w lemma="herein" pos="av" xml:id="A84108-001-a-3340">herein</w>
     <pc xml:id="A84108-001-a-3350">,</pc>
     <w lemma="i" pos="pns" xml:id="A84108-001-a-3360">I</w>
     <w lemma="shall" pos="vmb" xml:id="A84108-001-a-3370">shall</w>
     <w lemma="take" pos="vvi" xml:id="A84108-001-a-3380">take</w>
     <w lemma="it" pos="pn" xml:id="A84108-001-a-3390">it</w>
     <w lemma="for" pos="acp" xml:id="A84108-001-a-3400">for</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-3410">the</w>
     <w lemma="great" pos="js" xml:id="A84108-001-a-3420">greatest</w>
     <w lemma="honour" pos="n1" xml:id="A84108-001-a-3430">honour</w>
     <w lemma="that" pos="cs" xml:id="A84108-001-a-3440">that</w>
     <w lemma="have" pos="vvz" xml:id="A84108-001-a-3450">hath</w>
     <w lemma="befall" pos="vvn" reg="befallen" xml:id="A84108-001-a-3460">befalne</w>
     <w lemma="i" pos="pno" xml:id="A84108-001-a-3470">me</w>
     <pc xml:id="A84108-001-a-3480">,</pc>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-3490">and</w>
     <w lemma="will" pos="vmb" xml:id="A84108-001-a-3500">will</w>
     <w lemma="oblige" pos="vvi" xml:id="A84108-001-a-3510">oblige</w>
     <w lemma="myself" pos="pr" reg="myself" xml:id="A84108-001-a-3520">my selfe</w>
     <w lemma="to" pos="prt" xml:id="A84108-001-a-3540">to</w>
     <w lemma="acknowledge" pos="vvi" xml:id="A84108-001-a-3550">acknowledge</w>
     <w lemma="it" pos="pn" xml:id="A84108-001-a-3560">it</w>
     <w lemma="by" pos="acp" xml:id="A84108-001-a-3570">by</w>
     <w lemma="the" pos="d" xml:id="A84108-001-a-3580">the</w>
     <w lemma="utmost" pos="j" xml:id="A84108-001-a-3590">utmost</w>
     <w lemma="and" pos="cc" xml:id="A84108-001-a-3600">and</w>
     <w lemma="most" pos="avs-d" xml:id="A84108-001-a-3610">most</w>
     <w lemma="faithful" pos="j" reg="faithful" xml:id="A84108-001-a-3620">faithfull</w>
     <w lemma="endeavour" pos="n1" xml:id="A84108-001-a-3630">endeavour</w>
     <w lemma="of" pos="acp" xml:id="A84108-001-a-3640">of</w>
    </p>
    <closer xml:id="A84108-e110">
     <dateline xml:id="A84108-e120">
      <hi xml:id="A84108-e130">
       <w lemma="from" pos="acp" xml:id="A84108-001-a-3650">From</w>
       <w lemma="the" pos="d" xml:id="A84108-001-a-3660">the</w>
       <w lemma="rendezvouz" pos="n1" reg="rendezvouz" xml:id="A84108-001-a-3670">Rendez-vouz</w>
       <w lemma="at" pos="acp" xml:id="A84108-001-a-3680">at</w>
      </hi>
      <w lemma="Northampton" pos="nn1" reg="Northampton" xml:id="A84108-001-a-3690">Northamton</w>
      <pc xml:id="A84108-001-a-3700">,</pc>
      <date xml:id="A84108-e140">
       <w lemma="15." pos="crd" xml:id="A84108-001-a-3710">15.</w>
       <pc unit="sentence" xml:id="A84108-001-a-3720"/>
       <hi xml:id="A84108-e150">
        <w lemma="Septem." pos="ab" xml:id="A84108-001-a-3730">Septem.</w>
       </hi>
       <w lemma="1642." pos="crd" xml:id="A84108-001-a-3750">1642.</w>
       <pc unit="sentence" xml:id="A84108-001-a-3760"/>
      </date>
     </dateline>
     <signed xml:id="A84108-e160">
      <w lemma="your" pos="po" xml:id="A84108-001-a-3770">Your</w>
      <w lemma="faithful" pos="j" reg="faithful" xml:id="A84108-001-a-3780">faithfull</w>
      <w lemma="friend" pos="n1" xml:id="A84108-001-a-3790">friend</w>
      <w lemma="ESSEX" pos="nn1" xml:id="A84108-001-a-3800">ESSEX</w>
      <pc unit="sentence" xml:id="A84108-001-a-3810">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A84108-e170">
   <div type="colophon" xml:id="A84108-e180">
    <p xml:id="A84108-e190">
     <hi xml:id="A84108-e200">
      <w lemma="Septemb." pos="ab" xml:id="A84108-001-a-3820">Septemb.</w>
     </hi>
     <w lemma="19" pos="crd" xml:id="A84108-001-a-3830">19</w>
     <pc xml:id="A84108-001-a-3840">▪</pc>
     <w lemma="1642." pos="crd" xml:id="A84108-001-a-3850">1642.</w>
     <pc unit="sentence" xml:id="A84108-001-a-3860"/>
     <w lemma="london" pos="nn1" xml:id="A84108-001-a-3870">London</w>
     <pc xml:id="A84108-001-a-3880">,</pc>
     <w lemma="print" pos="vvn" xml:id="A84108-001-a-3890">printed</w>
     <w lemma="for" pos="acp" xml:id="A84108-001-a-3900">for</w>
     <hi xml:id="A84108-e210">
      <w lemma="William" pos="nn1" xml:id="A84108-001-a-3910">William</w>
      <w lemma="gay" pos="j" xml:id="A84108-001-a-3920">Gay</w>
      <pc unit="sentence" xml:id="A84108-001-a-3930">.</pc>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
